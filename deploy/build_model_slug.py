"""
blaze build //deploy:slug.json
"""

from absl import app
from absl import logging
from absl import flags

import json

_TEMPLATES_JSON = flags.DEFINE_string('templates_json', None, 'templates json')
_OUTPUT_FILE = flags.DEFINE_string('output_file', None, 'answer html')
_FIELDS = flags.DEFINE_string('fields', None, 'fields')
_CSS = flags.DEFINE_string('css', None, 'css file')
_LATEX_TEMPLATE = flags.DEFINE_string('latex_template', None,
                                      'LaTeX template file')
_TYPE = flags.DEFINE_enum('type', None, ['normal', 'cloze'], 'type')

_LATEX_SEPARATOR = 'HERE'


def read_file(path):
    with open(path) as f:
        return f.read()


def main(_):
    templates = {}
    for template in json.loads(_TEMPLATES_JSON.value)['templates']:
        human_name = template['human_name']
        if human_name in templates:
            raise Exception("duplicated human_name " + human_name)
        templates[human_name] = json.loads(read_file(template['slug']))
    with open(_OUTPUT_FILE.value, 'w') as f:
        latex_template = read_file(_LATEX_TEMPLATE.value)
        # TODO
        assert _LATEX_SEPARATOR in latex_template
        latex_pre, _, latex_post = latex_template.partition(_LATEX_SEPARATOR)
        assert latex_pre
        assert latex_post
        json.dump(
            {
                'css': read_file(_CSS.value),
                'latexPre': latex_pre,
                'latexPost': latex_post,
                'templates': templates,
                'fields': json.loads(_FIELDS.value)['fields'],
                'type': _TYPE.value,
            }, f)


if __name__ == '__main__':
    flags.mark_flags_as_required([
        _TEMPLATES_JSON.name,
        _OUTPUT_FILE.name,
        _CSS.name,
        _FIELDS.name,
        _TYPE.name,
        _LATEX_TEMPLATE.name,
    ])
    app.run(main)
