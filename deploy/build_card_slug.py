"""
blaze build //deploy:slug.json
"""

from absl import app
from absl import logging
from absl import flags

import json

_QUESTION_HTML_FILE = flags.DEFINE_string('question_html_file', None,
                                          'question html')
_ANSWER_HTML_FILE = flags.DEFINE_string('answer_html_file', None,
                                        'answer html')
_OUTPUT_FILE = flags.DEFINE_string('output_file', None, 'answer html')


def read_file(path):
    with open(path) as f:
        return f.read()


def main(_):
    with open(_OUTPUT_FILE.value, 'w') as f:
        json.dump(
            {
                'qfmt': read_file(_QUESTION_HTML_FILE.value),
                'afmt': read_file(_ANSWER_HTML_FILE.value)
            }, f)


if __name__ == '__main__':
    flags.mark_flags_as_required([
        _QUESTION_HTML_FILE.name,
        _ANSWER_HTML_FILE.name,
        _OUTPUT_FILE.name,
    ])
    app.run(main)
