"""
blaze run //deploy -- --alsologtostderr --collection_path=<...>

If all looks fine, run also --dry_run=False.

Example update slug:

  {
    '1234-abcd-crowdanki-uuid': {
      'css': '#card { ... } css { ... }',
      'templates': [
        'Card name': {
          'qfmt': 'HTML for question...',
          'afmt': 'HTML for answer...',
        }
      ]
    }
  }
"""

from absl import app
from absl import logging
from absl import flags

from anki_utils.deploy import deploy_lib
from rules_python.python.runfiles import runfiles

import anki
from anki.collection import Collection
import json
import yaml

_COLLECTION_PATH = flags.DEFINE_string('collection_path', None,
                                       'Path to .anki2 collection')
_CONFIG_YAML = flags.DEFINE_string('config_yaml', None, 'Config YAML')
_SLUG_PATH = flags.DEFINE_string('slug_path', None, 'Path to update JSON slug')
_DRY_RUN = flags.DEFINE_bool('dry_run', True, 'Whether to actually do it')
_ADD_IF_MISSING = flags.DEFINE_bool(
    'add_if_missing', False, 'Whether to add new models if they are missing')

SLUG_RUNFILES_PATH = 'anki_utils/models/slug.json'


def main(_):
    r = runfiles.Create()
    c = Collection(_COLLECTION_PATH.value)

    slug_path = _SLUG_PATH.value
    if slug_path is None:
        path = r.Rlocation(SLUG_RUNFILES_PATH)
        logging.info("Loading slug from runfiles: %s", path)
        slug_path = path

    with open(slug_path) as f:
        slug = json.load(f)

    with open(_CONFIG_YAML.value) as f:
        config = yaml.load(f, Loader=yaml.Loader)

    deploy_lib.apply_slug(c.models, config, slug, _ADD_IF_MISSING.value)

    if _DRY_RUN.value:
        logging.info("If all looks good, run with --nodry_run to commit.")
    else:
        c.save()
        logging.info("Changes were actually commited.")


if __name__ == '__main__':
    flags.mark_flags_as_required([_COLLECTION_PATH.name, _CONFIG_YAML.name])
    app.run(main)
