goog.module('agentydragon.resistorColorCodeExample.main');

const {Logger} = goog.require('agentydragon.logging');
const {ensureHeading} = goog.require('agentydragon.heading');
const {obtainNote} = goog.require('agentydragon.note');
const {renderResistorBands} = goog.require('agentydragon.resistorColorCodeExample.resistorColorCodeExample');

const note = obtainNote();
const logger = new Logger(note);
logger.installToConsole();
ensureHeading(logger, note);

const resistors = document.querySelectorAll('.render-as-resistor');
logger.log('render-as-resistor: ' + resistors.length);
for (const resistorRender of resistors) {
  const digits = resistorRender.getAttribute('data-digits');
  logger.log('rendering: [' + digits + ']');
  renderResistorBands(digits, resistorRender, logger);
}
