goog.module('agentydragon.resistorColorCodeExample.resistorColorCodeExample');

/**
 * Renders resistor bands (code in numberString) into
 * resistorContainer.
 */
function renderResistorBands(numberString, resistorContainer, logger) {
  resistorContainer.textContent = '';

  const resistorBandsList = document.createElement('ul');
  resistorBandsList.className = 'resistor-bands';

  numberString = numberString.trim();
  logger.log('numberString is: [' + numberString + ']');
  for (const digit of numberString) {
    function bandExpand(x) {
      if (x == '-') {
        return ' ';
      }
      if (x == 'S') {
        return 'S';
      }
      if (x == 'G') {
        return 'G';
      }
      if (isNaN(digit)) {
        return null;
      }
      return x;
    }
    const text = bandExpand(digit);
    logger.log('expand: ' + digit + ' -> ' + text);
    if (text == null) {
      logger.error(
          'invalid character in number string: [' + numberString + ']');
      return;
    }
    const inner = document.createElement('span');
    inner.innerText = text;
    const resistorBand = document.createElement('li');
    resistorBand.className = 'resistor-band-' + digit;
    resistorBand.append(inner);
    resistorBandsList.append(resistorBand);
  }
  resistorContainer.append(resistorBandsList);
}

exports = {renderResistorBands};
