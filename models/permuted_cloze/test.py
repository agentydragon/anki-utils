from absl.testing import absltest
from anki_utils.testing import card_testcase

FRONT_RUNFILES_PATH = "anki_utils/models/permuted_cloze/permuted_cloze_slug.json"


class PermutedClozeTest(card_testcase.CardTestCase):
    def load_front_card(self, text, log=False, heading='Heading'):
        self.load_model(FRONT_RUNFILES_PATH)
        fields = {
            'Text': text,
            'Log': 'true' if log else '',
            'Heading': heading
        }
        self.open_card(fields, render_answer=True)

    def make_items(self, n=10):
        return ["item {}".format(i) for i in range(1, n + 1)]

    def make_list(self, items):
        return '<ul>' + '\n'.join(('<li>' + item for item in items)) + '</ul>'

    def assertIsPermutation(self, actual, expected):
        self.assertEqual(set(actual), set(expected), "item set not preserved")
        self.assertNotEqual(actual, expected, "item ordering not permuted")

    def test_front_permutation_ul_items(self):
        """Tests that <ul> items 1..20 in <li> tags get permuted."""
        items = self.make_items(20)
        self.load_front_card(text="{{c1::foo}} " + self.make_list(items))
        texts = [
            item.text for item in self.driver.find_elements_by_tag_name('li')
        ]
        self.assertIsPermutation(texts, items)

    def test_front_permutation_br_divided_lines(self):
        """Tests that <br>-divided items 1..20 get permuted."""
        items = self.make_items(20)
        self.load_front_card(text="{{c1::foo}}<br>" + '<br>'.join(items))
        actual_text = self.driver.find_element_by_id(
            'agentydragon-content').text
        actual_lines = actual_text.strip().split('\n')
        actual_lines.remove('foo')
        self.assertIsPermutation(actual_lines, items)

    def test_front_permutation_tbody_rows(self):
        """Tests that permuting rows of a table body."""
        items = self.make_items(20)
        html = ('<table><thead><tr><th>Header</tr></thead><tbody>' + ''.join(
            map(lambda item: '<tr><td>' + item + '</td></tr>', items)) +
                '</tbody></table>')
        self.load_front_card(text="{{c1::foo}} " + html)
        rows = self.driver.find_elements_by_css_selector(
            '#agentydragon-content tr')
        actual_lines = [row.text.strip() for row in rows]
        # "Header" should remain the first line.
        self.assertEqual(actual_lines[0], "Header")
        # The rest should be permuted.
        self.assertIsPermutation(actual_lines[1:], items)

    def test_log(self):
        """Tests that Log set to 'true' enables logging."""
        items = self.make_items(2)
        self.load_front_card(text="{{c1::foo}} " + self.make_list(items),
                             log=True)
        self.assertIn('shuffling', self.get_log())

    def test_report_error_on_no_container(self):
        """Tests that if there is no container to permute, an error is reported."""
        self.load_front_card(text='{{c1::foo}} Hello World!')
        self.assertIn('No permuted container', self.get_log())

    # TODO(prvak): Test that front and back permutation for the same card is
    # the same.


if __name__ == "__main__":
    absltest.main()
