# Heading

My cards have headings, usually showing the general area of the card.

If a note has `Heading` field and it has non-empty content, the content will
be shown in the heading of cards from that note.

Otherwise, headings are computed from the note's tags. See
[heading.js](/shared_styles/heading.js) and
[heading_test.js](/shared_styles/heading_test.js].
