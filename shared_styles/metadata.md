# Metadata

The following fields are not shown on any cards.

## Improvement tags

### `TODO`

I use this field to note down any issues I notice with the card while reviewing
that I might not want to immediately fix - for example, because I'm on my phone.
For example, "LaTeX renders too wide on phone, need to scroll horizontally".

### `Ambiguous`

If I notice that I confused the answer to the card with something else that
is in some sense very close to the right answer. For example, when I answer with
a word that sounds similar.

## Sources

### `Sources`

Into this field I put whatever source I got the information from. For example,
a Wikipedia URL, or even the URL to a Google search, or the name of a book.

### `Roam refs`

I paste references to my personal [Roam Research](https://roamresearch.com/)
database into this field. Typically those are just naked block references like
`((d3adB3Ef))`. Currently this doesn't do anything, but maybe some day it will.

