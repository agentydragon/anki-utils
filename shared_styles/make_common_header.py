from absl import app
from absl import logging
from absl import flags

import json

_FIELDS = flags.DEFINE_string('fields', None,
                              'json string, field names the model has')
_OUTPUT_FILE = flags.DEFINE_string('output_file', None, 'output file')

_KNOWN_FIELDS = {'Heading', 'Seed', 'Log'}
_ALWAYS_PRESENT_FIELDS = {'Deck', 'Tags', 'Type', 'Card'}

# Static prologue.
# Includes shared MathJax definitions.
_PROLOGUE = """
<div id="agentydragon-prologue">
  \(
    % \E: expected value, accepts limits
    \DeclareMathOperator*\E{\mathbb{E} }
    % \kl: KL divergence
    % TODO: make KL divergence also take parameters to auto-render \parallel
    \def\kl{\mathcal{D}_\mathrm{KL} }
  \)
</div>
<div id="agentydragon-log"></div>
"""


def main(_):
    fields = json.loads(_FIELDS.value)['fields']
    with open(_OUTPUT_FILE.value, 'w') as f:
        f.write('<div id="agentydragon-fields">\n')
        # TODO: create bazel build constants for those field names

        # Only propagate fields that are "well-known", and fields that are
        # always present.
        for field in ((set(fields) & _KNOWN_FIELDS) | _ALWAYS_PRESENT_FIELDS):
            f.write('  <span data-field="' + field + '">{{' + field +
                    '}}</span>\n')

        f.write('</div>\n')
        f.write(_PROLOGUE)


if __name__ == '__main__':
    flags.mark_flags_as_required([_FIELDS.name, _OUTPUT_FILE.name])
    app.run(main)
