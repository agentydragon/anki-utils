"""
bazel run :make_gpt_dataset -- \
  --out_jsonl_path=$(pwd)/out.jsonl \
  --collection_path=/home/agentydragon/drive/anki/agentydragon/collection.anki2
"""

from absl import app
from absl import flags

import re
import anki
from anki.collection import Collection

import json

_COLLECTION_PATH = flags.DEFINE_string('collection_path', None,
                                       'Path to .anki2 collection')
_OUT_JSONL_PATH = flags.DEFINE_string('out_jsonl_path', None,
                                      'Path to output jsonl')


def main(_):
    c = Collection(_COLLECTION_PATH.value)
    note_ids = c.find_notes(query='deck:All -tag:_secret -tag:persons*')
    with open(_OUT_JSONL_PATH.value, 'w') as f:
        for nid in note_ids:
            note = c.get_note(nid)
            model_name = c.models.get(note.mid)['name']

            note.tags

            if model_name in ('Cloze', 'Permuted Cloze'):
                completion = note['Text']
                completion = completion.replace('&nbsp;', ' ')
                completion = completion.strip()
                completion = ' ' + completion + ' END'

                # TODO: idea: strip out syntax highlighting from completion?
                prompts = note['GPTPrompt'].strip()
                prompts = re.split('\n|<br>', prompts)
                SEPARATOR = '---\n'
                if prompts and not (len(prompts) == 1 and prompts[0] == ''):
                    for prompt in prompts:
                        prompt = prompt.replace('&nbsp;', ' ')
                        prompt = prompt.strip()
                        if not prompt:
                            continue
                        # TODO: make unnecessary?
                        json.dump(
                            {
                                'prompt': prompt + '\n' + SEPARATOR,
                                'completion': completion,
                            }, f)
                        f.write('\n')
                else:
                    json.dump({
                        'prompt': SEPARATOR,
                        'completion': completion,
                    }, f)
                    f.write('\n')
            else:
                continue
                # raise Exception(f"unhandled model: {model_name}")


if __name__ == '__main__':
    flags.mark_flags_as_required([_COLLECTION_PATH.name, _OUT_JSONL_PATH.name])
    app.run(main)
